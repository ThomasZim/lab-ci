#ifndef __INTEGER_AVG_H
#define __INTEGER_AVG_H

/** @return The average of two integers @ref a and @ref b. */
int integer_average(int a, int b);

#endif //__INTEGER_AVG_H