#include "integer_avg.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>

void setUp(void)
{
    /* Set stuff up here */
}

void tearDown(void)
{
    /* Clean stuff up here */
}

void test_average_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_average(0, 0), "Error in integer_average");
    TEST_ASSERT_EQUAL_INT_MESSAGE(3, integer_average(2, 4), "Error in integer_average");
TEST_ASSERT_EQUAL_INT_MESSAGE(3, integer_average(2, 5), "Error in integer_average");



}

